import threading
import time

class MyThread(threading.Thread):
    def run(self):
        print(
            "Hello from {}!".format(
                self.getName()
            )
        )
        # Thread execution is spaced out by at least 1 seconds.
        time.sleep(1)
        print(
            "{} finidhed!".format(
                self.getName()
            )
        )


def main():
    for x in range(4):
        mythread = MyThread(
            name = "Thread-{}".format(
                x + 1
            )
        )
        mythread.start()
        # Thread creation is spaced out by at least 0.8 seconds.
        time.sleep(0.8)


if __name__ == '__main__':
    main()
