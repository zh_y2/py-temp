def sum(lst):
    if len(lst) == 1:
        return lst[-1]
    return (lst[-1] + sum(lst[:-1]))

def Fib(n):
    if n == 1:
        return 1
    if n == 2:
        return 1
    for i in range(n):
        return (Fib(n-1) + Fib(n-2))
    
lst01 = [2,5,6,7,98,23,51,86]
print(sum(lst01))

print(Fib(1))
print(Fib(2))
print(Fib(10))
