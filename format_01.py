#!/usr/bin/python
age = 20
name = "Saeroop"
print('{0} was {1} years old when he wrote this book.'.format(name, age))
print("Why is {0} play whis that python?".format(name))

# 花括号内增加 : 、并紧跟格式要求可指定显示格式。如下，浮点数保留小数点后3位
print('{0:.3f}'.format(1.0 / 3))
# 使用下划线填充文本、并保持文字处于中间位置，使用 ^ 定义字符串长度
print('{0:_^11}'.format("Hello"))
# 基于关键词输出
print("{name} wrote {book}.".format(name='Swaroop', book="A Byte of Python"))

# print 结尾默认有换行字符，留意以下语句不指定换行符也会换行
print('标准', end='\n')
print('结尾')
print('在这里')

# 现在指定 print 语句不换行
print('a', end='')
print('b', end='')
print('c')
print('Modern', end=" ")
print('artist')

# 转义字符：以反斜线 \ 开头
print('What\'s your name?')

# 转义字符 \n 换行 \t 制表 \ 换行显示但不添加新行（类似 Word 文档的 Shift+Enter）
print('This is the first line \n This is the second line')
print('This is the first line \ This is the second line')
print('This is the first line \t This is the second line')

# Raw string 字符串前加'r'，不处理任何特殊字符
print(r'This is the first line \n This is the second line')
