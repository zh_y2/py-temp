#!/user/bin/python3
# -*- coding: utf-8 -*-

class Employee:
    "Common base class for all employees."
    empCount = 0;

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Employee.empCount += 1

    def displayCount(self):
        print("Total employee %d" % Employee.empCount)

    def displayEmployee(self):
        print('Name: ', self.name, ", salary: ", self.salary)

# This would create first object of Employee class
emp1 = Employee("Zara", 5000)
# This would create second object of Employee class
emp2 = Employee("Manni", 6000)

# Noww show the objects
emp1.displayEmployee()
emp2.displayEmployee()
print("Total employee is %d" % Employee.empCount)